<?php

namespace App\Http\Controllers\Admin;

use App\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    public function index()
    {
        $data['galleries'] = Gallery::orderBy('id', 'desc')->paginate(20);
        return view('adminlte::gallery.gallery_list', $data);
    }

    public function add()
    {
        return view('adminlte::gallery.gallery_add');
    }

    public function addPost(Request $request)
    {
        //dd($request->all());
        $gallery = new Gallery();
        $gallery->match_played = $request->get('match_played');
        $gallery->image = $request->get('image');
        $gallery->showing_from = date('Y-m-d H:i:s', strtotime($request->get('showing_from')));
        $gallery->showing_to = date('Y-m-d H:i:s', strtotime($request->get('showing_to')));
        $gallery->result = $request->get('result');
        $gallery->is_active = $request->get('is_active');
        $gallery->save();
        return redirect()->back()->with('message', 'Added Success');
    }

    public function editPost($galleryId, Request $request)
    {
        //dd($request->all());
        $gallery = Banner::find($galleryId);
        $gallery->match_played = $request->get('match_played');
        $gallery->image = $request->get('image');
        $gallery->showing_from = date('Y-m-d H:i:s', strtotime($request->get('showing_from')));
        $gallery->showing_to = date('Y-m-d H:i:s', strtotime($request->get('showing_to')));
        $gallery->is_active = $request->get('is_active');
        $gallery->result = $request->get('result');
        $gallery->save();
        return redirect()->back()->with('message', 'Added Success');
    }

    public function edit($galleryId)
    {
        $data['gallery'] = Gallery::where('id', $galleryId)->first()->toArray();
        return view('adminlte::gallery.gallery_edit', $data);
    }//
}
