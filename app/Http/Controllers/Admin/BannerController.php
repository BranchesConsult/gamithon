<?php

namespace App\Http\Controllers\Admin;

use App\Banner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{
    public function index()
    {
        $data['banners'] = Banner::orderBy('id', 'desc')->paginate(20);
        return view('adminlte::banners.banner_list', $data);
    }

    public function add()
    {
        return view('adminlte::banners.banner_add');
    }

    public function addPost(Request $request)
    {
        //dd($request->all());
        $banner = new Banner();
        $banner->title = $request->get('title');
        $banner->url = $request->get('url');
        $banner->showing_from = date('Y-m-d H:i:s', strtotime($request->get('showing_from')));
        $banner->showing_to = date('Y-m-d H:i:s', strtotime($request->get('showing_to')));
        $banner->is_active = $request->get('is_active');
        $banner->save();
        return redirect()->back()->with('message', 'Added Success');
    }

    public function editPost($bannerId, Request $request)
    {
        //dd($request->all());
        $banner = Banner::find($bannerId);
        $banner->title = $request->get('title');
        $banner->url = $request->get('url');
        $banner->showing_from = date('Y-m-d H:i:s', strtotime($request->get('showing_from')));
        $banner->showing_to = date('Y-m-d H:i:s', strtotime($request->get('showing_to')));
        $banner->is_active = $request->get('is_active');
        $banner->save();
        return redirect()->back()->with('message', 'Added Success');
    }

    public function edit($bannerId)
    {
        $data['banner'] = Banner::where('id', $bannerId)->first()->toArray();
        return view('adminlte::banners.banner_edit', $data);
    }
}
