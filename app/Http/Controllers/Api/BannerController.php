<?php

namespace App\Http\Controllers\Api;

use App\Banner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{
    public function index()
    {
        $banners = Banner::select(['url'])->where('is_active', 1)->
        where('showing_from', '<=', getGmtTime())
            ->where('showing_to', '>=', getGmtTime())->get()->toArray();
        return response()
            ->json(['status' => true, 'banners' => $banners]);
    }
}
