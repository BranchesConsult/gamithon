<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
        'showing_to',
        'showing_from',
    ];
}
