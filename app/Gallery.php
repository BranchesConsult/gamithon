<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'gallery';
    protected $dates = [
        'created_at',
        'updated_at',
        'showing_to',
        'showing_from',
    ];
}
