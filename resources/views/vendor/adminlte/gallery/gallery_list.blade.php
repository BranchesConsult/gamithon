@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Games
@endsection
@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Banners</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tr>
                                    <th>ID</th>
                                    <th>Match</th>
                                    <th>Url</th>
                                    <th>From (GMT)</th>
                                    <th>To (GMT)</th>
                                    <th>Status</th>
                                    <th colspan="3">Actions</th>
                                </tr>
                                @foreach($galleries as $key => $gallery)
                                    <tr>
                                        <td>{!! $gallery['id'] !!}</td>
                                        <td>{!! $gallery['match_played'] !!}</td>
                                        <td><a href="{!! $gallery['image'] !!}" target="_blank">View</a></td>
                                        <td>{!! $gallery['showing_from'] !!}</td>
                                        <td>{!! $gallery['showing_to'] !!}</td>
                                        <td>{!! $gallery['is_active'] !!}</td>
                                        <td>
                                            <a class="btn btn-warning"
                                               href="{!! route('galleryEdit', ['gallery_id'=>$gallery['id']]) !!}">
                                                Edit
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        {!! $galleries->links() !!}
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@endsection