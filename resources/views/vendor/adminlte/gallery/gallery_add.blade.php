@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Games
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ad Gallery</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['url' => route('galleryAddPost')]) !!}
                        <div class="form-group">
                            <label>Match</label>
                            <input class="form-control" name="match_played" required/>
                        </div>
                        <div class="form-group">
                            <label>Result</label>
                            <input class="form-control" name="result" required/>
                        </div>
                        <div class="form-group">
                            <label>Show From (GMT)</label>
                            <input class="form-control datetimepicker" name="showing_from" required/>
                        </div>
                        <div class="form-group">
                            <label>Show To (GMT)</label>
                            <input class="form-control datetimepicker" name="showing_to" required/>
                        </div>
                        @for($i = 0;$i < 1; $i++)
                            <div class="form-group">
                                <div class="input-append">
                                    <input class="form-control" id="post_image_{{$i}}" value=""
                                           name="image" type="text">
                                    <a data-toggle="modal" href="javascript:;" data-target="#galleryModal_{{$i}}"
                                       class="btn btn-default" type="button">
                                        Select
                                    </a>
                                </div>
                            </div>
                        @endfor
                        <div class="form-group">
                            <label>Active</label>
                            <select class="form-control" name="is_active">
                                <option value="">Please select</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input class="btn btn-primary" type="submit" value="Save"/>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @for($i = 0;$i < 1; $i++)
        <div class="modal fade" id="galleryModal_{{$i}}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Add post</h4>
                    </div>
                    <div class="modal-body">
                        <iframe width="100%" height="800"
                                src="{{URL::to("filemanager/dialog.php?type=1&field_id=post_image_".$i)}}"
                                frameborder="0"
                                style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @endfor

@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('/datepicker/jquery.datetimepicker.css')}}">
@stop
@section('js')
    {!! Html::script('js/tinymce/tinymce.min.js') !!}
    {!! Html::script('js/tinymce.js') !!}
    <script src="{{asset('/datepicker/jquery.datetimepicker.js')}}"></script>
    <script src="{{asset('/datepicker/jquery.datatimefull.js')}}"></script>
    <script>
        $(document).ready(function ($) {
            var datePickerTheme = 'default';
            var dateFormat = 'd.m.y';
            var timeFormat = 'H:i';
            var timeStep = 15;
            //DateTime picker
            $(".datetimepicker").datetimepicker({
                minDate: '0', //yesterday is minimum date(for today use 0 or -1970/01/01)
                // maxDate: '+1970/01/10',
                //step: timeStep,
                //minTime: 0,
                hours12: false,
                theme: datePickerTheme
            });
        });
    </script>
@stop