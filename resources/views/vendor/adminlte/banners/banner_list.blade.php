@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Games
@endsection
@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Banners</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tr>
                                    <th>Banner ID</th>
                                    <th>Title</th>
                                    <th>Url</th>
                                    <th>From (GMT)</th>
                                    <th>To (GMT)</th>
                                    <th>Status</th>
                                    <th colspan="3">Actions</th>
                                </tr>
                                @foreach($banners as $key => $banner)
                                    <tr>
                                        <td>{!! $banner['id'] !!}</td>
                                        <td>{!! $banner['title'] !!}</td>
                                        <td><a href="{!! $banner['url'] !!}" target="_blank">View</a></td>
                                        <td>{!! $banner['showing_from'] !!}</td>
                                        <td>{!! $banner['showing_to'] !!}</td>
                                        <td>{!! $banner['is_active'] !!}</td>
                                        <td>
                                            <a class="btn btn-warning"
                                               href="{!! route('bannerEdit', ['banner_id'=>$banner['id']]) !!}">
                                                Edit
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@endsection