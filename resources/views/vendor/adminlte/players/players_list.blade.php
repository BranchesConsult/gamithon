@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Games
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List of Players</h3>
                        <div class="pull-right">
                            {!! Form::open(['url' => route('playerslist'),'method'=>'GET']) !!}
                            <input autocomplete="fasle" class="form-control" value="{!! @$searchedPlayer !!}"
                                   type="text"
                                   name="searched_player"
                                   placeholder="player_name"/>
                            <input autocomplete="fasle" class="form-control" value="{!! @$searchedTournament !!}"
                                   type="text"
                                   name="tournament_id"
                                   placeholder="Tornament id"/>
                            <input type="submit" name="searchPlayer" class="btn btn-primary" value="search"/>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tr>
                                    <th>Player Name</th>
                                    <th>Image</th>
                                    <th>Crick api id</th>
                                    <th colspan="3">Actions</th>
                                </tr>
                                @foreach($player_list as $row)

                                    <tr>
                                        <td>
                                            {{$row['name']}}
                                        </td>
                                        <td>
                                            <img style="width: 50px; height: 50px;"
                                                 src="{{URL::to('uploads/'.$row['profile_pic'])}}" alt="N/A"/>
                                            <br/>
                                            URL: {!! !empty($row['profile_pic']) ? URL::to('uploads/'.$row['profile_pic']) : 'N/a' !!}
                                        </td>
                                        <td>
                                            {{$row['cricapi_pid']}}
                                        </td>
                                        <td>
                                            <a href="{{route('editPlayerForm', ['player_id'=>$row['id']])}}"
                                               class="btn btn-warning">
                                                <i class="fa fa-pencil"></i> Edit
                                            </a>
                                            <a href="#" class="btn btn-danger">
                                                <i class="fa fa-times"></i> Delete
                                            </a>
                                            <a href="{{route('showAddStatForm', ['player_id'=>$row['id']])}}"
                                               class="btn btn-danger">
                                                <i class="fa fa-times"></i> Player Rankings
                                            </a>
                                            <a href="{{route('addPlayerStats', ['player_id'=>$row['id']])}}"
                                               class="btn btn-success">
                                                <i class="fa fa-times"></i> Player Stats
                                            </a>
                                            <a href="{{route('editPlayerStats', ['player_id'=>$row['id']])}}"
                                               class="btn btn-success">
                                                <i class="fa fa-times"></i> Edit Stats
                                            </a>


                                        </td>


                                    </tr>
                                @endforeach

                            </table>
                        </div>
                        {{ $player_list->appends(request()->except('page'))->links() }}
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@endsection